# PROJETO ARQUITETURAL

## RESUMO

<!-- O resumo deve apresentar, de forma sucinta, os pontos relevantes do trabalho em um parágrafo único e espaço entre linhas de 1,5. O resumo deve conter a apresentação do problema, uma descrição sucinta da aplicação e aspectos relevantes da sua arquitetura. O resumo também deve apresentar evidências da avaliação arquitetural. Ele deve conter de 100 a 250 palavras. -->

Na modalidade de vendas por _dropshipping_ o lojista não mantém o estoque dos produtos ofertados. Quando o pedido de um cliente é aprovado, o lojista encomenda o produto ao fornecedor, que fica responsável por entregar o produto diretamente para o cliente. 

Essa modalidade vem sendo utilizada largamente nos comércios eletrônicos nos dias atuais. Seu sucesso se dá, principalmente, porque os lojistas precisam de um capital muito menor pois não precisam gerenciar o estoque físico e todos os custos provenientes de funcionários.

Porém, para que essa modalidade funcione bem é necessário uma comunicação muito eficaz entre o lojista, que disponibiliza o produto para o cliente final, e o fornecedor, que mantém o estoque e faz a entrega desse produto.

A proposta é criar uma solução que seja capaz de atender a todos os requisitos para que tudo funcione. Desde a divulgação e controle dos produtos ofertados para o cliente, a comunicação com o fornecedor e o acompanhamento da entrega do produto.

> Palavras-chave: dropshipping. venda. comércio online. web. sem estoque. 

## SUMÁRIO

### 1. Objetivos do trabalho

<!-- Aqui você deve descrever os objetivos do trabalho indicando que o objetivo geral é apresentar a descrição do projeto arquitetural da aplicação escolhida. Apresente também alguns (pelo menos 3) objetivos específicos dependendo de onde você vai querer concentrar a sua descrição arquitetural, ou como você vai aprofundar no seu trabalho. -->

Este trabalho tem como objetivo geral a elaboração de um projeto arquitetural de uma solução para realizar vendas na modalidade _dropshipping_ no comércio eletrônico. Para que todo o processo de venda e entrega aconteça corretamente é imprescindível que o sistema do lojista seja integrado ao sistema do fornecedor e que os interessados sejam avisados sobre qualquer evento que ocorra com o produto.

Abaixo os objetivos específicos do projeto:

1. Desenvolver o módulo de vendas que possibilite o cliente efetuar a compra de um produto. Este módulo também inclui a comunicação com o fornecedor para que o processo de entrega se inicie.

2. Desenvolver um controle logística que seja responsável por registrar todos os eventos relacionados ao produto. Toda alteração será comunicada aos envolvidos para que o processo seja transparente.

3. Enviar diariamente informações sobre vendas, eventos e avaliações para o sistema de BI.

Justificativa das escolhas: apesar de todos os itens serem importantes para o sucesso completo da solução, escolhi os que considerei mais críticos em relação ao tema dropshipping. Interpretei o módulo de Controle de devolução, reenvio e extravio como sendo um subconjunto do Controle de logística, por isso não o escolhi. Já os módulos de Propaganda e promoção, SAC e Relatórios de acompanhamento são preocupações comuns em outros contextos além do _dropshipping_, por esse motivo existe uma grande probabilidade de existirem soluções focadas nesses assuntos, não sendo necessário implementá-las e sim integrar com uma solução já desenvolvida.

### 2. Descrição geral da solução

#### 2.1. Apresentação do problema

<!-- Aqui você deve descrever o problema onde o software está inserido. Apresente os problemas existentes no processo e o problema que a sua aplicação deve resolver. Porém, não é ainda a hora de comentar sobre a aplicação. 
Descreva também o contexto em que essa aplicação será usada, se houver: empresa, tecnologias, etc. Novamente, descreva apenas o que de fato existir, pois ainda não é a hora de apresentar requisitos detalhados ou projetos. -->

A modalidade dropshipping tem crescido muito nos últimos anos, um dos principais atrativos desse modelo é o baixo custo inicial para abrir uma loja virtual. Esse baixo custo se dá pela possibilidade do lojista vender os produtos para seus clientes sem de fato gerenciar estoque, tudo é encaminhado para um fornecedor que tem um estoque gerenciado e faz a entrega para o cliente.

Essa comunicação entre lojista, fornecedor e cliente é muito importante para que as operações ocorram com sucesso. Sempre que um cliente faz uma compra o fornecedor deve receber o pedido, uma falha nessa comunicação pode inviabilizar o negócio. Outra preocupração que deve ser considerada para uma boa experiência do cliente é o acompanhamento em tempo real de todos os eventos que o produto está sujeito.

Visando uma boa experiência para o usuário a aplicacação será disponibilizada na web e será responsiva, de forma que seja possível acessar de um celular ou computador.

#### 2.2. Descrição geral do software

<!-- Apresente os objetivos do software e uma descrição geral da solução. -->

A solução se propõe em oferecer um ambiente seguro e confiável em que todos os desafios apontados sejam considerados e tratados corretamente. Para oferecer uma boa experiência para o cliente, o sistema será disponibilizado na web e estará disponível 24x7. As informações sobre a situação dos produtos serão gerenciadas por um módulo automático que mantém uma comunicação em tempo real com as empresas envolvidas no processo de entrega do produto.

Para que a empresa tome decisões acertivas e mais eficazes, informações sobre as vendas e avaliações dos clientes são enviadas para um sistema especializado em BI. Os dados são enviados diariamente, dessa forma os gestores tem acesso quase que em tempo real sobre as vendas e entregas e podem identificar problemas rapidamente.

Sabendo que os sistemas dos fornecedores estão implantados em outra infraestrutura, as integrações com esses sistemas serão desenhadas de forma a mitigar eventuais problemas transientes de comunicação como falha de conexão ou instabilidade na rede. Considerando também que as integrações com os fornecedores podem variar de protocolo e tecnologia, as regras de negócio terão uma separação bem definida em relação aos detalhes de comunicação com esses sistemas externos.

As tecnologias, linguagens de programação e frameworks utilizadas são estáveis e possuem atualizações de segurança.

### 3. Definição conceitual da solução

#### 3.1. Requisitos Funcionais

<!-- Enumere os requisitos funcionais previstos para a sua aplicação. Concentre-se nos requisitos funcionais que sejam críticos para a definição arquitetural. Lembre-se de listar todos os requisitos funcionais que são necessários para garantir cobertura arquitetural. Ou seja, requisitos ou cenários que auxiliam na validação final da arquitetura. Esta seção deve conter uma lista de requisitos ainda sem modelagem. Se for útil, separe os requisitos por módulos. -->

##### Módulo Vendas

- Lista de produtos
O sistema deve disponibilizar ao cliente final uma página em que ele possa efetuar a compra de um produto.

- Comunicação fornecedor
Sempre que uma venda é realizada o forcenecedor deve receber o pedido.

##### Módulo Logística

- Registro de enventos
Todos os eventos devem ser registrados para acompanhamento.

- Notificação de eventos
O cliente deve ser notificado quando ocorrer uma mudança de situação.

##### Módulo integração BI

- Integração

- Envio diário
Diariamente as informações de vendas devem ser enviadas.

#### 3.2. Requisitos Não-Funcionais

<!-- Enumere os requisitos não-funcionais previstos para a sua aplicação. Entre os requisitos não funcionais, inclua todos os requisitos que julgar importante do ponto de vista arquitetural ou seja os requisitos que terão impacto na definição da arquitetura. Os requisitos não funcionais devem ser descritos no padrão estímulo-resposta. -->

Fonte do Estímulo | 
Estímulo | 
Ambiente | 
Artefato | 
Resposta |
Medida da Resposta | 

Seguem abaixo os requisitos não funcionais do sistema proposto:

• O sistema deve prover boa usabilidade.

* Usabilidade

Fonte do Estímulo | Cliente interessado em comprar um produto  
Estímulo | Acesso a página de vendas, escolha dos produtos e finalização  
Ambiente | Operação normal  
Artefato | Módulo de vendas  
Resposta | Interface intuitiva e simples  
Medida da Resposta | Compra finaliza em até 7 minutos.


• O sistema deve suportar ambientes Web responsivos e ambientes móveis.

* Adaptabilidade

Fonte do Estímulo | Cliente acessando página de compras pelo celular
Estímulo | Acesso ao sistema pelo celular  
Ambiente | Operação normal  
Artefato | Módulo de vendas  
Resposta | Elementos dispostos de acordo com a resolução do celular
Medida da Resposta | Nenhuma rolagem horizontal, elementos secundários escondidos e elementos redimensionados.

• O sistema deve ser rápido.

* Desempenho

Fonte do Estímulo | Cliente acessando página de compras  
Estímulo | Acesso a página de produtos com estoque  
Ambiente | Operação normal  
Artefato | Módulo de vendas  
Resposta | Tela apresentado ao cliente
Medida da Resposta | Lista de produtos exibida em até 3 segundos

• O sistema deve apresentar manutenção facilitada.
• O sistema deve ser simples para testar.

* Testabilidade

Fonte do Estímulo | Desenvolvedor  
Estímulo | Execução dos testes  
Ambiente | Ambiente de desenvolvimento  
Artefato | Código fonte do módulo de vendas  
Resposta | Resultado dos testes (ok ou falha)
Medida da Resposta | Execução dos testes com apenas um comando  

• O sistema deve se comunicar com os sistemas dos fornecedores. Alguns desses sistemas são antigos e desenvolvido com tecnologia COBOL/CICS.

* Interoperabilidade

Fonte do Estímulo | Modulo de vendas  
Estímulo | Finalização de uma compra  
Ambiente | Operação normal  
Artefato | Módulo de vendas  
Resposta | Envio do pedido para o fornecedor  
Medida da Resposta | Confirmação de recebimento do fornecedor  

• O sistema deve operar em qualquer período do dia e da noite.

* Disponibilidade

Fonte do Estímulo | Administrador dos servidores  
Estímulo | Alteração das configurações do servidor  
Ambiente | Ambiente de produção, operação normal  
Artefato | Balanceador de carga  
Resposta | Servidores com a configuração antiga desligados e novos servidores provisionados com as configurações mais recentes  
Medida da Resposta | Nenhuma requisição deixar de ser atendida, as requisições para os servidores antigos são finalizadas antes do seu desligamento e novas requisições são direcionadas para os novos servidores  

• O sistema deve apresentar altos padrões de segurança.

* Segurança

Fonte do Estímulo | Administrador do site  
Estímulo | Acesso as configurações do site sem antes estar autenticado  
Ambiente | Operação normal  
Artefato | Módulo de integração BI  
Resposta | Redirecionamento para tela de login  
Medida da Resposta | Nenhuma informação privada exibida  


• O sistema deve estar disponível 24 horas por dia nos sete dias da semana.

#### 3.3. Restrições Arquiteturais

<!-- Enumere as restrições arquiteturais. Lembre-se de que as restrições arquiteturais geralmente não são consideradas requisitos uma vez que limitam a solução candidata. Os requisitos não impõem restrição, mas precisam ser satisfeitos. -->

- deve-se utilizar a linguagem PHP;
- deve-se disponibilizar o sistema na WEB;
- todas as comunicações com as integrações devem ser feitas por meios seguros;
- deve-se utilizar um banco de dados relacional.

#### 3.4. Mecanismos Arquiteturais

<!-- Inclua nesta seção os mecanismos arquiteturais de análise, projeto e implementação da arquitetura inicialmente contemplada para a sua aplicação. -->

Mecanismo de Análise | Mecanismo de Design | Mecanismo de Implementação
--- | --- | ---  
Sistema Operacional | Ambiente de execução da aplicação | Ubuntu
Versionamento | Gerenciamento das mudanças no código fonte | Git
Autenticação | Identificação sem compartilhamento de senha | OAuth2
Persistência | Bando de dados relacional | MariaDB
Cache | Uso em memória | Redis
Execução em segundo plano | 
Agendamento | 

### 4. Modelagem e projeto arquitetural

#### 4.1. Modelo de casos de uso

<!-- O diagrama de casos de uso oferece uma visão global dos casos de uso e dos atores que dele participam.  Ele deve contemplar todos requisitos funcionais. -->

opções: draw.io; cloud.smartdraw.com;

Módulos adicionais:

- Cadastro de produtos
- Cadastro de fornecedores

#### 4.2. Descrição resumida dos casos de uso

<!-- Faça uma  descrição resumida de cada caso de uso. -->
<!--! Pode ser também no formato de estórias. -->

utilizar o padrão who, what, why
utilizar o método INVEST para avaliar a história

Como consumidor gostaria de buscar um produto para que eu compre-o.
Como consumidor necessito receber notificações sobre o produto que eu comprei para que eu me mantenha informado.
Como gestor preciso gerar relatório das avaliações para saber a satisfação dos consumidores.
Como consumidor gostaria de registrar minha avaliação do produto para que a empresa tome conhecimento sobre minha experiência.
Como administrador gostaria de cadastrar promoções para aumentar as vendas.
Como administrador necessito encerrar uma promoção para que os preços voltem ao normal.
Como gestor necessito gerar relatório de rentabilidade para avaliar a saúda financeira da empresa.
Como consumidor preciso enviar uma solicitação para que meu problema seja resolvido.
Como atendente do suporte necessito atualizar a sitação de uma solicitação para que o consumidor fique ciente sobre a situação da solicitação.


#### 4.3. Modelo de componentes

<!-- Apresente o diagrama de componentes da aplicação, indicando, os elementos da arquitetura e as interfaces entre eles. Liste os estilos/padrões arquiteturais utilizados e faça uma descrição sucinta dos componentes indicando o papel de cada um deles dentro da arquitetura/estilo/padrão arquitetural. Indique também quais componentes serão reutilizados (navegadores, SGBDs, middlewares, etc), quais componentes serão adquiridos por serem proprietários e quais componentes precisam ser desenvolvidos. -->

#### 4.4. Modelo de implantação

<!-- Apresente o diagrama de implantação da aplicação, indicando, o mapeamento dos elementos de software da arquitetura para os elementos de hardware onde eles executarão. Apresente a caracterização completa dos elementos de hardware necessários para a execução dos elementos de software. -->

#### 4.5. Modelo de dados (opcional)

### 5. Prova de conceito / protótipo arquitetural 



#### 5.1. Implementação e implantação

<!-- Descreva a implementação da prova de conceito da arquitetura (protótipo arquitetural) da sua aplicação. 
Indique as tecnologias utilizadas na implementação.
Indique os casos de uso implementados que serão usados para validar a arquitetura proposta. Deve se pelo menos três casos de uso. Apresente as telas usadas.
Indique os requisitos não funcionais que serão avaliados. Devem ser pelo menos três requisites não funcionais.  
 Faça a implantação da sua prova de conceito (nuvem, servidor web, aplicativo para ser baixado para o um smartphone). Indique onde sua prova de conceito está disponível para ser executada. 
Ao final faça um vídeo de apresentação da POC e disponibilize de forma que a banca de avaliação do TCC possa ver. Informe a URL no apêndice. -->

- linguagem: php
- framework: laravel
- banco de dados: mariadb
- implantação: heroku ou aws (beanstalk)

#### 5.2. Interfaces/ APIs

<!-- Caso exista algum componente na arquitetura da sua aplicação que é genérico e pode ser usado em outras aplicações semelhantes, documente a interface deste componente seguindo um modelo de documentação de interfaces. -->

- fornecedores!
- rastreamento.
- transportadora?

### 6. Avaliação da Arquitetura

#### 6.1. Análise das abordagens arquiteturais

<!-- Apresente um breve resumo das principais características da proposta arquitetura. -->

#### 6.2. Identificação dos atributos de qualidade

<!-- Identifique e priorize os mais  atributos de qualidade mais importantes. -->

#### 6.3. Cenários

<!-- Apresente os cenários de testes utilizados na realização dos testes da sua aplicação. Escolha cenários de testes que demonstre os requisitos não funcionais (atributos de qualidade) sendo satisfeitos. Priorize os cenários para a avaliação. -->

#### 6.4. Avaliação

<!-- Apresente as evidências dos testes de avaliação. Apresente as medidas registradas na coleta de dados. O que não for possível quantificar apresente uma justificativa baseada em evidências qualitativas que suportam o atendimento do requisito não-funcional. As evidências das avaliações neste item são fundamentais. -->

- riscos: 
* dados gerencias desatualizados


#### 6.5. Resultado

<!-- Apresente uma avaliação geral da arquitetura indicando os pontos fortes e as limitações da arquitetura proposta. Indique possíveis ajustes que devem ser feitos. -->

### 7. Conclusão

<!-- Faça uma avaliação geral do trabalho. Indique se os objetivos foram atendidos, as limitações do resultado e as dificuldades encontradas.  -->

## REFERÊNCIAS

<!-- Como um projeto da arquitetura de uma aplicação não requer revisão bibliográfica, a inclusão das referências não é obrigatória. No entanto, caso você deseje incluir referências relacionadas às tecnologias, padrões, ou metodologias que serão usadas no seu trabalho, relacione-as de acordo com o modelo a seguir. Usar as normas ABNT para listar as referência.
Exemplo:
SOBRENOME DO AUTOR, Nome do autor. Título do livro ou artigo. Cidade: Editora, ano. -->

## APÊNDICES

<!-- Inclua o URL do repositório (Github, Bitbucket, etc) onde você armazenou o código da sua prova de conceito/protótipo arquitetural da aplicação como anexos. A inclusão da URL desse repositório de código servirá como base para garantir a autenticidade dos trabalhos.

Inclua o URL do vídeo mostrando uma apresentação da POC.  -->